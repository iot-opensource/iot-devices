from datetime import datetime
Import("env")


def increaseVersion(version):
    print(version)
    parts = version.split('"')
    v = parts[1].split('.')
    v[1] = str(int(v[1]) + 1)

    return f"#define FIRMWARE_BUILD_VERSION \"{v[0]}.{v[1]}\"\n"

def updateFile(pathToFile):
    file = open(pathToFile, "r+")
    
    newLines = []

    for line in file:
        print(line)
        if line.find("#define FIRMWARE_BUILD_VERSION") != -1:
            newLines.append(increaseVersion(line))
        elif line.find("#define FIRMWARE_BUILD_DATE") != -1:
            date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            newLines.append(f"#define FIRMWARE_BUILD_DATE \"{date}\"\n")
        else:
            newLines.append(line)
        
    file.seek(0)
    file.truncate()
    file.writelines(newLines)
    file.close()

project = str(env["PROJECT_DIR"])
updateFile(f"{project}/include/consts.h")
