#include "ConfigEepromRepository.h"

ConfigEepromRepository::ConfigEepromRepository()
{
}

ConfigEepromRepository::~ConfigEepromRepository()
{
}

void ConfigEepromRepository::save(Config* config)
{
    EepromStorage.begin();

    EepromStorage.set<float>("humidity-threshold", config->getHumidityThreshold());
    EepromStorage.set<float>("temp-threshold", config->getTemperatureThreshold());
    EepromStorage.set<OperationMode>("operation-mode", config->getOperationMode());

    EepromStorage.apply();
}

Config* ConfigEepromRepository::read()
{
    EepromStorage.begin();

    if (EepromStorage.exists("humidity-threshold")) {
        return new Config(
            EepromStorage.get<float>("humidity-threshold"),
            EepromStorage.get<float>("temp-threshold"),
            EepromStorage.get<OperationMode>("operation-mode")
        );
    }

    return new Config(
        0,
        0,
        OperationMode::MANUAL
    );
}