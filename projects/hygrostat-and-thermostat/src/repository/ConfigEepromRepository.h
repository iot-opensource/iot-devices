#ifndef __CONFIG_EEPROM_REPOSITORY_H__
#define __CONFIG_EEPROM_REPOSITORY_H__

#include <Arduino.h>
#include <EepromRepository.h>
#include "../models/Config.h"

class ConfigEepromRepository
{
    public:
        ConfigEepromRepository();
        ~ConfigEepromRepository();
        static void save(Config* config);
        static Config* read();
};

#endif