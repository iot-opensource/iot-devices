#ifndef __DEFAULT_CONTROLLER_H__
#define __DEFAULT_CONTROLLER_H__

#ifndef Arduino_h
#include <Arduino.h>
#endif
#include <ESPAsyncWebServer.h>
#include <DHT.h>
#include <ArduinoJson.h>
#include "../models/DeviceGroup.h"
#include "../models/Config.h"
#include "consts.h"

class DefaultController
{
    DeviceGroup *actuators;
    Config *config;
    DHT *humiditySensor;

public:
    DefaultController(DHT *humiditySensor, DeviceGroup *actuators, Config *config);
    ~DefaultController();
    void mainRoute(AsyncWebServerRequest *request);
    void propertiesRoute(AsyncWebServerRequest *request);
    void postDeviceRoute(AsyncWebServerRequest *request);
};

#endif