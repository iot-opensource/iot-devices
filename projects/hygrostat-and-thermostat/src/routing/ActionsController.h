#ifndef __ACTIONS_CONTROLLER_H__
#define __ACTIONS_CONTROLLER_H__

#ifndef Arduino_h
#include <Arduino.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include <AsyncJson.h>
#include <Logger.h>
#include "consts.h"
#include "../models/Config.h"
#include "../models/DeviceGroup.h"
#include "../repository/ConfigEepromRepository.h"

class ActionsController
{
    Config *config;
    DeviceGroup *actuators;

public:
    ActionsController(Config *config, DeviceGroup *actuators);
    ~ActionsController();
    void onOffAction(AsyncWebServerRequest *request, DynamicJsonDocument body);
    void changeModeAction(AsyncWebServerRequest *request, DynamicJsonDocument body);
};

#endif