#include "ConfigController.h"

ConfigController::ConfigController(Config *config)
{
    this->config = config;
}

ConfigController::~ConfigController()
{
}

void ConfigController::getRoute(AsyncWebServerRequest *request)
{
    Config *configTmp = ConfigEepromRepository::read();

    DynamicJsonDocument json(1024);
    json["humidityThreshold"] = String(configTmp->getHumidityThreshold());
    json["temperatureThreshold"] = String(configTmp->getTemperatureThreshold());
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);

    delete configTmp;
}

void ConfigController::patchRoute(AsyncWebServerRequest *request, DynamicJsonDocument body)
{
    if (body.containsKey("humidityThreshold")) {
        float humidityThreshold = body["humidityThreshold"].as<float>();

        if (!this->config->changeHumidityTreshold(humidityThreshold)) {
            return this->badResponse(request, "Invalid humidity threshold");
        }
    }

    if (body.containsKey("temperatureThreshold")) {
        float temperatureThreshold = body["temperatureThreshold"].as<float>();

        if (!this->config->changeTemperatureTreshold(temperatureThreshold)) {
            return this->badResponse(request, "Invalid temperature threshold");
        }
    }

    ConfigEepromRepository::save(this->config);

    this->getRoute(request);
}

void ConfigController::badResponse(AsyncWebServerRequest *request, String message) {
    DynamicJsonDocument error(1024);
    error["error"]["message"] = message;
    String parsedJson;
    serializeJson(error, parsedJson);
    
    request->send(400, "application/json", parsedJson);
}