#ifndef __CONFIG_CONTROLLER_H__
#define __CONFIG_CONTROLLER_H__

#ifndef Arduino_h
#include <Arduino.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include "consts.h"
#include "../models/Config.h"
#include "../repository/ConfigEepromRepository.h"

class ConfigController
{
    Config *config;

public:
    ConfigController(Config *config);
    ~ConfigController();
    void getRoute(AsyncWebServerRequest *request);
    void patchRoute(AsyncWebServerRequest *request, DynamicJsonDocument body);
    void badResponse(AsyncWebServerRequest *request, String message);
};

#endif