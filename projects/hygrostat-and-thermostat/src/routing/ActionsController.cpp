#include "ActionsController.h"

ActionsController::ActionsController(Config *config, DeviceGroup *actuators)
{
    this->config = config;
    this->actuators = actuators;
}

ActionsController::~ActionsController()
{
}

void ActionsController::onOffAction(AsyncWebServerRequest *request, DynamicJsonDocument body)
{
    if (!body.containsKey("value")) {
        request->send(400, "application/json", "{\"status\":\"error\"}");
        return;
    }

    bool status = body["value"].as<bool>();

    if (status) {
        this->actuators->enable();
        Logger.info("Device enabled");
    } else {
        this->actuators->disable();
        Logger.info("Device disabled");
    }

    request->send(204);
}

void ActionsController::changeModeAction(AsyncWebServerRequest *request, DynamicJsonDocument body)
{
    if (!body.containsKey("value"))
    {
        request->send(400, "application/json", "{\"status\":\"error\"}");
        return;
    }

    String mode = body["value"];

    if (mode == "thermostat") {
        this->config->changeOperationMode(THERMOSTAT);
    } else if (mode == "manual") {
        this->config->changeOperationMode(MANUAL);
    } else if (mode == "hygrostat") {
        this->config->changeOperationMode(HYGROSTAT);
    } else {
        DynamicJsonDocument json(1024);
        json["status"] = "error";
        json["message"] = "Value '' is not correct. Posible values are: thermostat, hygrostat, manual";
        String parsedJson;
        serializeJson(json, parsedJson);
        request->send(400, "application/json", parsedJson);
    }

    ConfigEepromRepository::save(this->config);

    request->send(204);
}