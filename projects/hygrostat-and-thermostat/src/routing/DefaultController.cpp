#include "DefaultController.h"

DefaultController::DefaultController(DHT *humiditySensor, DeviceGroup *actuators, Config *config)
{
    this->humiditySensor = humiditySensor;
    this->actuators = actuators;
    this->config = config;
}

DefaultController::~DefaultController()
{
}

void DefaultController::mainRoute(AsyncWebServerRequest *request)
{
    DynamicJsonDocument json(1024);
    json["name"] = "Thermostat & hygrostat";
    json["description"] = "";
    json["apiUrl"] = "/api/v1";
    json["firmwareVersion"] = FIRMWARE_BUILD_VERSION;
    json["firmwareBuildDate"] = FIRMWARE_BUILD_DATE;
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "text/plain", parsedJson);
}

void DefaultController::propertiesRoute(AsyncWebServerRequest *request)
{
    DynamicJsonDocument json(1024);

    json["status"]["value"] = this->actuators->isEnabled() ? "on" : "off";
    json["humidity"]["value"] = String(this->humiditySensor->readHumidity());
    json["humidity"]["unit"] = "%";
    json["temperature"]["value"] = String(this->humiditySensor->readTemperature());
    json["temperature"]["unit"] = "°C";
    json["mode"]["value"] = String(this->config->operationModeAsString());

    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}
