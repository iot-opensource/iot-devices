#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Adafruit_Sensor.h>
#include <ArduinoJson.h>
#include <WiFiManager.h>
#include <Logger.h>
#include <persistance-systems/InMemorySystem.h>
#include <AsyncElegantOTA.h>
#include <DHT.h>
#include <EepromRepository.h>
#include "repository/ConfigEepromRepository.h"
#include "../include/consts.h"
#include "routing/routing.h"
#include "models/models.h"

DHT *humiditySensor;
Device *deviceOne;
Device *led;
DeviceGroup *actuators;
AsyncWebServer *httpServer;
DefaultController *defaultController;
ConfigController *configController;
ActionsController *actionsController;
Config *config;

int pushButtonTime = 0;

void notFound(AsyncWebServerRequest *request)
{
    request->send(404, "text/plain", "Not found");
}

void badResponse(AsyncWebServerRequest *request) {
    DynamicJsonDocument json(1024);
    json["status"] = "error";
    json["message"] = "Invalid JSON";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(400, "application/json", parsedJson);
}

void setup()
{
    int startingTime = millis();

    Serial.begin(115200, SERIAL_8N1, SERIAL_TX_ONLY);

    Logger.begin(LogLevel::TRACE, new InMemorySystem());
    Logger.useSerial(&Serial);

    Logger.info("Hygrostat & Thermostat");

    deviceOne = new Device(DEVICE_ONE_PIN);
    led = new Device(LED_PIN);
    config = ConfigEepromRepository::read();
    httpServer = new AsyncWebServer(HTTP_SERVER_PORT);
    humiditySensor = new DHT(DHTPIN, DHTTYPE);

    vector<IDevice*> devices = {
        led,
        deviceOne,
    };
    actuators = new DeviceGroup(devices);

    pinMode(SWITCH_PIN, INPUT_PULLUP);

    humiditySensor->begin();

    defaultController = new DefaultController(humiditySensor, actuators, config);
    configController = new ConfigController(
        config);
    actionsController = new ActionsController(
        config,
        actuators);

    WiFiManager.begin(httpServer);

    if (WiFiManager.isConnected()) {
        DateTime.begin();

        Logger.useEndpoints(httpServer, "/logs");
        Logger.useDatetime(&DateTime);

        httpServer->on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
            defaultController->mainRoute(request);
        });
    }

    httpServer->on("/api/v1/properties", HTTP_GET, [](AsyncWebServerRequest *request)
                { defaultController->propertiesRoute(request); });
    httpServer->on(
        "/api/v1/actions/onOff", 
        HTTP_POST, 
        [](AsyncWebServerRequest *request) {},
        NULL,
        [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total)
        {
            String body = String((char *)data);

            DynamicJsonDocument doc(1024);
            DeserializationError error = deserializeJson(doc, body);

            if (error)
            {
                badResponse(request);
                return;
            }

            actionsController->onOffAction(request, doc); 
        }
    );
    httpServer->on(
        "/api/v1/actions/changeMode", 
        HTTP_POST, 
        [](AsyncWebServerRequest *request) {},
        NULL,
        [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total)
        {
            String body = String((char *)data);

            DynamicJsonDocument doc(1024);
            DeserializationError error = deserializeJson(doc, body);

            if (error)
            {
                badResponse(request);
                return;
            }

            actionsController->changeModeAction(request, doc); 
        }
    );
    httpServer->on(
        "/api/v1/config", 
        HTTP_GET, 
        [](AsyncWebServerRequest *request) { 
            configController->getRoute(request); 
        }
    );
    httpServer->on(
        "/api/v1/config", 
        HTTP_PATCH, 
        [](AsyncWebServerRequest *request) {},
        NULL,
        [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total)
        {
            String body = String((char *)data);

            DynamicJsonDocument doc(1024);
            DeserializationError error = deserializeJson(doc, body);

            if (error)
            {
                badResponse(request);
                return;
            }

            configController->patchRoute(request, doc); 
        }
    );

    httpServer->onNotFound(notFound);
    AsyncElegantOTA.begin(httpServer, OTA_USERNAME, OTA_PASSWORD);
    httpServer->begin();

    Logger.info("Program started. Duration in millisec: %d", millis() - startingTime);
}

void loop()
{
    if (config->getOperationMode() == HYGROSTAT)
    {
        float humidity = humiditySensor->readHumidity();

        if (humidity + HUMIDITY_HYSTERESIS > config->getHumidityThreshold())
        {
            actuators->enable();
        }
        else if (humidity - HUMIDITY_HYSTERESIS < config->getHumidityThreshold())
        {
            actuators->disable();
        }
    }
    else if (config->getOperationMode() == THERMOSTAT)
    {
        float temperature = humiditySensor->readTemperature();

        if (temperature - TEMPERATURE_HYSTERESIS > config->getTemperatureThreshold())
        {
            actuators->disable();
        }
        else if (temperature + TEMPERATURE_HYSTERESIS < config->getTemperatureThreshold())
        {
            deviceOne->enable();
        }
    }

    if(digitalRead(SWITCH_PIN) == LOW)
    {
        if (!pushButtonTime) {
            pushButtonTime = millis() / 1000; // seconds
        }

        if ((millis() / 1000) - pushButtonTime >= 5) {
            EepromStorage.clear();
            led->enable();
            delay(1000);
            led->disable();

            #if defined(ESP8266) || defined(ESP32)
                ESP.restart();
            #else
                resetItSelf();
            #endif
        }
    } else {
        if (pushButtonTime) {
            actuators->isEnabled() ? actuators->disable() : actuators->enable();
            config->changeOperationMode(MANUAL);
        }

        pushButtonTime = 0;
    }
}