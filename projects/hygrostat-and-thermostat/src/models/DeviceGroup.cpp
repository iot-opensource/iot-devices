#include "DeviceGroup.h"

DeviceGroup::DeviceGroup(vector<IDevice*> devices) {
    this->devices = devices;
}

DeviceGroup::~DeviceGroup() {}

void DeviceGroup::enable() {
    for (int i = 0; i < this->devices.size(); i++) {
        this->devices[i]->enable();
    }
}

void DeviceGroup::disable() {
    for (int i = 0; i < this->devices.size(); i++) {
        this->devices[i]->disable();
    }
}

bool DeviceGroup::isEnabled() {
    for (int i = 0; i < this->devices.size(); i++) {
        if (this->devices[i]->isEnabled()) {
            return true;
        }
    }

    return false;
}

