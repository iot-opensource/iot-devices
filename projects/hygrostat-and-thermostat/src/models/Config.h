#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <Arduino.h>

enum OperationMode
{
    HYGROSTAT = 0,
    MANUAL = 1,
    THERMOSTAT = 2
};

class Config
{
	float humidityThreshold;
    float temperatureThreshold;
    OperationMode operationMode;

public:
	Config(float humidityThreshold, float temperatureThreshold, OperationMode operationMode);
	~Config();
    float getHumidityThreshold();
    float getTemperatureThreshold();
    OperationMode getOperationMode();
    bool changeHumidityTreshold(float humidityThreshold);
    bool changeTemperatureTreshold(float temperatureThreshold);
    void changeOperationMode(OperationMode operationMode);
    String operationModeAsString();
};

#endif
