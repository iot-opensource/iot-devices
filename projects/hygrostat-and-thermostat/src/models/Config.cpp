#include "Config.h"

Config::Config(float humidityThreshold, float temperatureThreshold, OperationMode operationMode)
{
	this->humidityThreshold = humidityThreshold;
    this->temperatureThreshold = temperatureThreshold;
	this->operationMode = operationMode;
}

Config::~Config()
{
}

float Config::getHumidityThreshold() {
	return this->humidityThreshold;
}

float Config::getTemperatureThreshold() {
	return this->temperatureThreshold;
}

OperationMode Config::getOperationMode() {
	return this->operationMode;
}

bool Config::changeHumidityTreshold(float value) {
	if (value < 0 || value > 99.0) return false;

    this->humidityThreshold = value;
    return true;
}

bool Config::changeTemperatureTreshold(float value) {
    if (value < -100.0 || value > 999.0) return false;

    this->temperatureThreshold = value;
    return true;
}

void Config::changeOperationMode(OperationMode operationMode) {
	this->operationMode = operationMode;
}

String Config::operationModeAsString() {
    String mode = "unknown";

    if (this->operationMode == MANUAL) {
        mode = "manual";
    } else if (this->operationMode == THERMOSTAT) {
        mode = "thermostat";
    } else if (this->operationMode == HYGROSTAT) {
        mode = "hygrostat";
    }

    return mode;
}

