#ifndef __DEVICE_H__
#define __DEVICE_H__

#include <Arduino.h>
#include "IDevice.h"

class Device : public IDevice
{
	uint8_t pin;
    bool run;

public:
	Device(uint8_t pin);
	~Device();
    void enable();
    void disable();
    bool isEnabled();
};

#endif //__DEVICE_H__
