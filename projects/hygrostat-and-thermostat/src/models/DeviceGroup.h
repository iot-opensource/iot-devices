#ifndef __DEVICE_GROUP_H__
#define __DEVICE_GROUP_H__

#include <Arduino.h>
#include "IDevice.h"
#include <vector>

using namespace std;

class DeviceGroup : public IDevice {
    vector<IDevice*> devices;

public:
    DeviceGroup(vector<IDevice*> devices);
    ~DeviceGroup();
    void enable();
    void disable();
    bool isEnabled();
};

#endif