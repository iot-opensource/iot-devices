#ifndef __IDEVICE_H__
#define __IDEVICE_H__

class IDevice
{
public:
	virtual ~IDevice() {};
    virtual void enable() = 0;
    virtual void disable() = 0;
    virtual bool isEnabled() = 0;
};

#endif //__IDEVICE_H__
