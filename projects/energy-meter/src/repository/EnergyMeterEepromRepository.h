#ifndef __ENERGY_METER_EEPROM_REPOSITORY_H__
#define __ENERGY_METER_EEPROM_REPOSITORY_H__

#include <Arduino.h>
#include <EEPROM.h>
#include "../models/EnergyMeter.h"
#include "../../include/consts.h"

class EnergyMeterEepromRepository
{
    public:
        EnergyMeterEepromRepository();
        ~EnergyMeterEepromRepository();
        static void save(EnergyMeter* energyMeter);
        static EnergyMeter* read();
};

#endif