#include "EnergyMeterEepromRepository.h"

EnergyMeterEepromRepository::EnergyMeterEepromRepository()
{
}

EnergyMeterEepromRepository::~EnergyMeterEepromRepository()
{
}

void EnergyMeterEepromRepository::save(EnergyMeter* energyMeter)
{
    EEPROM.put(0, energyMeter->getValue());
    
    if (EEPROM.commit() == true) {
        Serial.println("Dane poprawnie zapisane");
    } else {
        Serial.println("Error during save config to EEPROM");
    }
}

EnergyMeter* EnergyMeterEepromRepository::read()
{
    int valueFromEeprom;

    EEPROM.get(0, valueFromEeprom);

    return new EnergyMeter(
        IMPULS_PIN,
        valueFromEeprom
    );
}