#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Hash.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <WiFiManager.h>
#include <AsyncElegantOTA.h>
#include "../include/consts.h"
#include "routing/DefaultController.h"
#include "models/EnergyMeter.h"
#include <EepromRepository.h>

EnergyMeter *energyMeter;
AsyncWebServer *httpServer;
DefaultController *defaultController;

void log(String message)
{
    Serial.println(message);
}

void notFound(AsyncWebServerRequest *request)
{
    request->send(404, "text/plain", "Not found");
}

void setup()
{
    Serial.begin(9600);
    EepromStorage.begin();

    long unsigned int initValue = 0;

    if (EepromStorage.exists("value")) {
        initValue = EepromStorage.get<long unsigned int>("value");
    }

    energyMeter = new EnergyMeter(IMPULS_PIN, initValue);
    httpServer = new AsyncWebServer(HTTP_SERVER_PORT);
    defaultController = new DefaultController(energyMeter);

    log("Energy Meter IoT");

    WiFiManager.begin(httpServer);

    if (WiFiManager.isConnected()) {
        httpServer->on("/", [](AsyncWebServerRequest *request)
                    { defaultController->mainRoute(request); });
        httpServer->on("/api/v1/energy", HTTP_GET, [](AsyncWebServerRequest *request)
                    { defaultController->energyRoute(request); });
        httpServer->on("/api/v1/properties", HTTP_GET, [](AsyncWebServerRequest *request)
                    { defaultController->propertiesRoute(request); });
        httpServer->on(
            "/api/v1/actions/changeValue",
            HTTP_POST,
            [](AsyncWebServerRequest *request) {},
            NULL,
            [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total)
            {
                String body = String((char *)data);

                Serial.println(body);

                DynamicJsonDocument doc(1024);
                DeserializationError error = deserializeJson(doc, body);

                if (error)
                {
                    DynamicJsonDocument json(1024);
                    json["status"] = "error";
                    json["message"] = "Invalid JSON";
                    String parsedJson;
                    serializeJson(json, parsedJson);

                    request->send(400, "application/json", parsedJson);
                    return;
                }

                defaultController->changeValueActionRoute(request, doc);
          });
    }

    httpServer->onNotFound(notFound);
    AsyncElegantOTA.begin(httpServer, OTA_USERNAME, OTA_PASSWORD);
    httpServer->begin();
}

void loop()
{
    if (digitalRead(IMPULS_PIN) == LOW)
    {
        energyMeter->increaseValue();
        EepromStorage.set<long unsigned int>("value", energyMeter->getValue());

        delay(100);
    }
}