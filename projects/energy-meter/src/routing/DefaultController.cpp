#include "DefaultController.h"

DefaultController::DefaultController(EnergyMeter *energyMeter)
{
    this->energyMeter = energyMeter;
}

DefaultController::~DefaultController()
{
}

void DefaultController::mainRoute(AsyncWebServerRequest *request)
{
    DynamicJsonDocument json(1024);
    json["name"] = "Energy meter";
    json["description"] = "";
    json["apiUrl"] = "/api/v1";
    json["firmwareVersion"] = FIRMWARE_BUILD_VERSION;
    json["firmwareBuildDate"] = FIRMWARE_BUILD_DATE;
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}

void DefaultController::energyRoute(AsyncWebServerRequest *request)
{
    DynamicJsonDocument json(1024);
    json["value"] = String(this->energyMeter->getValue());
    json["unit"] = "Wh";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}

void DefaultController::propertiesRoute(AsyncWebServerRequest *request)
{
    DynamicJsonDocument json(1024);
    json["energy"]["value"] = String(this->energyMeter->getValue());
    json["energy"]["unit"] = "Wh";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}

void DefaultController::changeValueActionRoute(AsyncWebServerRequest *request, DynamicJsonDocument body)
{
    if (body.containsKey("value"))
    {
        EepromStorage.begin();

        String value = body["value"];

        this->energyMeter->changeValue(value.toInt());

        EepromStorage.set<long unsigned int>("value", this->energyMeter->getValue());
        EepromStorage.apply();

        Serial.println("Energy value was corettly changed");

        body["value"] = String(this->energyMeter->getValue());

        String parsedJson;
        serializeJson(body, parsedJson);

        request->send(200, "application/json", parsedJson);
        return;
    }

    request->send(400, "application/json", "{\"status\":\"value is required\"}");
}
