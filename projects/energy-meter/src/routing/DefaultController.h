#ifndef __DEFAULT_CONTROLLER_H__
#define __DEFAULT_CONTROLLER_H__

#ifndef Arduino_h
#include <Arduino.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include <AsyncJson.h>
#include <EepromRepository.h>
#include "../models/EnergyMeter.h"
#include "consts.h"

class DefaultController
{
    EnergyMeter *energyMeter;

public:
    DefaultController(EnergyMeter *energyMeter);
    ~DefaultController();
    void mainRoute(AsyncWebServerRequest *request);
    void energyRoute(AsyncWebServerRequest *request);
    void propertiesRoute(AsyncWebServerRequest *request);
    void changeValueActionRoute(AsyncWebServerRequest *request, DynamicJsonDocument body);
};

#endif