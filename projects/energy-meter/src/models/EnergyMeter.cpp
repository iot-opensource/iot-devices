#include "EnergyMeter.h"

EnergyMeter::EnergyMeter(uint8_t pin, long unsigned int initialValue)
{
    this->pin = pin;
    this->value = initialValue;

    pinMode(pin, INPUT_PULLUP);
}

EnergyMeter::~EnergyMeter()
{
}

void EnergyMeter::changeValue(long unsigned int value) {
    this->value = value;
}

void EnergyMeter::increaseValue() {
    this->value += 1;
}

long unsigned int EnergyMeter::getValue() {
    return this->value;
}

