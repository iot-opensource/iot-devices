#ifndef __ENERGY_METER_H__
#define __ENERGY_METER_H__

#include <Arduino.h>
#include <EepromRepository.h>

class EnergyMeter
{
	uint8_t pin;
    long unsigned int value;

public:
	EnergyMeter(uint8_t pin, long unsigned int initialValue = 0);
	~EnergyMeter();
    void changeValue(long unsigned int value);
    void increaseValue();
    long unsigned int getValue();
};

#endif //__ENERGY_METER_H__
