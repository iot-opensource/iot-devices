## Commands
Upload project to device:
`pio run -t upload`

To erase all memory (flash + eeprom):
`pio run -t erase`

To open serial monitor:
`pio device monitor`

## DHT with AsyncWebServer
AsyncWebServer nie pozwala na używanie `yield()` w kodzie i użycie tego spowoduje exception. Niestety biblioteka DHT używa tego w swoim kodzie w pliku `DHT sensor library/DHT.cpp:244` trzeba przed skompilowaniem kodu to zakomentować.

## If platformio does not see devices
[Source](https://unix.stackexchange.com/questions/670636/unable-to-use-usb-dongle-based-on-usb-serial-converter-chip)

`sudo systemctl mask brltty.path