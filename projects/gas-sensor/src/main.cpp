#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Hash.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <WiFiManager.h>
#include "../include/consts.h"
#include "../include/helpers.h"
#include "routing/DefaultController.h"
#include "models/GasSensor.h"
#include "models/Device.h"

DHT* humiditySensor;
AsyncWebServer* httpServer;
DefaultController* defaultController;
GasSensor* mq2GasSensor;
Device* siren;
Device* greenLed;

float temperature = 0.0;
float humidity = 0.0;
int pushButtonTime = 0;

const char* PARAM_MESSAGE = "message";

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}

void setup() {
    Serial.begin(9600);

    // Serial.setDebugOutput(true);

    mq2GasSensor = new GasSensor(GAS_SENSOR_ANALOG, GAS_SENSOR_DIGIT);
    httpServer = new AsyncWebServer(HTTP_SERVER_PORT);
    humiditySensor = new DHT(DHTPIN, DHTTYPE);
    defaultController = new DefaultController(humiditySensor, mq2GasSensor);
    siren = new Device(SIREN_PIN);
    greenLed = new Device(GREEN_LED_PIN);

    humiditySensor->begin();

    WiFiManager.begin(httpServer);

    httpServer->on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        defaultController->mainRoute(request);
    });
    httpServer->on("/api/v1/clear", HTTP_GET, [](AsyncWebServerRequest *request) {
        EepromStorage.clear();
        request->send(200, "text/html", "clear");
    });
    httpServer->on("/api/v1/temperature", HTTP_GET, [](AsyncWebServerRequest *request) {
        defaultController->temperatureRoute(request, temperature); 
    });
    httpServer->on("/api/v1/humidity", HTTP_GET, [](AsyncWebServerRequest *request) {
        defaultController->humidityRoute(request, humidity); 
    });
    httpServer->on("/api/v1/gas", HTTP_GET, [](AsyncWebServerRequest *request) {
        defaultController->gasRoute(request); 
    });

    httpServer->onNotFound(notFound);
    AsyncElegantOTA.begin(httpServer, OTA_USERNAME, OTA_PASSWORD);
    httpServer->begin();

    greenLed->enable();

    pinMode(BUTTON_RESET, INPUT_PULLUP);
}

void loop() {
  temperature = humiditySensor->readTemperature();
  humidity = humiditySensor->readHumidity();

  if (mq2GasSensor->isGasDetected()) {
      siren->enable();
  } else {
      siren->disable();
  }

  if(digitalRead(BUTTON_RESET) == LOW)
    {
        if (!pushButtonTime) {
            pushButtonTime = millis() / 1000; // seconds
        }

        if ((millis() / 1000) - pushButtonTime >= 5) {
            EepromStorage.clear();
            siren->enable();
            delay(200);
            siren->disable();
            
            #if defined(ESP8266) || defined(ESP32)
                ESP.restart();
            #else
                resetItSelf();
            #endif
        }
    } else {
        pushButtonTime = 0;
    }

}