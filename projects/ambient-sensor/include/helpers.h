#ifndef __HELPERS_H__
#define __HELPERS_H__

#if !defined(ESP8266) && !defined(ESP32)
    // auto restart
    inline void(* resetItself) (void) = 0;
#endif

#endif