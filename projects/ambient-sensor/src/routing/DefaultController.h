#ifndef __DEFAULT_CONTROLLER_H__
#define __DEFAULT_CONTROLLER_H__

#ifndef Arduino_h
    #include <Arduino.h>
#endif
#include <DHT.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include "consts.h"

class DefaultController
{
public:
	DefaultController();
	~DefaultController();
    void mainRoute(AsyncWebServerRequest* request);
    void temperatureRoute(AsyncWebServerRequest* request, float temperature);
    void humidityRoute(AsyncWebServerRequest* request, float humidity);
    void propertiesRoute(AsyncWebServerRequest* request, float temperature, float humidity, int brightness);
};

#endif