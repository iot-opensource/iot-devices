#include "DefaultController.h"

DefaultController::DefaultController()
{
}

DefaultController::~DefaultController() {}

void DefaultController::mainRoute(AsyncWebServerRequest* request)
{
    DynamicJsonDocument json(1024);
    json["name"] = "Ambient sensor";
    json["description"] = "";
    json["apiUrl"] = "/api/v1";
    json["firmwareVersion"] = FIRMWARE_BUILD_VERSION;
    json["firmwareBuildDate"] = FIRMWARE_BUILD_DATE;
    String parsedJson;
    serializeJson(json, parsedJson);
    
    request->send(200, "application/json", parsedJson);

}

void DefaultController::temperatureRoute(AsyncWebServerRequest* request, float temperature)
{
    DynamicJsonDocument json(1024);
    json["value"] = String(temperature);
    json["unit"] = "°C";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}

void DefaultController::humidityRoute(AsyncWebServerRequest* request, float humidity)
{
    DynamicJsonDocument json(1024);
    json["value"] = String(humidity);
    json["unit"] = "%";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}

void DefaultController::propertiesRoute(AsyncWebServerRequest* request, float temperature, float humidity, int brightness)
{
    DynamicJsonDocument json(1024);

    json["temperature"]["value"] = String(temperature);
    json["temperature"]["unit"] = "°C";
    json["humidity"]["value"] = String(humidity);
    json["humidity"]["unit"] = "%";
    json["brightness"]["value"] = String(brightness);
    json["brightness"]["unit"] = "%";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}