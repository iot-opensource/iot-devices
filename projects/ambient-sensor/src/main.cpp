#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <SSD1306Wire.h>
#include <Hash.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <WiFiManager.h>
#include "../include/consts.h"
#include "../include/fonts.h"
#include "../include/helpers.h"
#include "routing/DefaultController.h"

DHT* humiditySensor;
AsyncWebServer* httpServer;
DefaultController* defaultController;
SSD1306Wire *oled;

float temperature = 0.0;
float humidity = 0.0;
int brightness = 50; // it's percent value from 0 to 100%
int pushButtonTime = 0;

const char* PARAM_MESSAGE = "message";

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}
void drawValues(int a, int b) {
    oled->clear();
    oled->setBrightness(brightness);
    oled->setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
    oled->drawString(64, 32, String(a) + "°C " + String(b) + "%");
    oled->display();
}
void setup() {
    Serial.begin(9600);

    humiditySensor = new DHT(DHTPIN, DHTTYPE);
    humiditySensor->begin();

    httpServer = new AsyncWebServer(HTTP_SERVER_PORT);
    defaultController = new DefaultController();
    oled = new SSD1306Wire(0x3c, SDA, SCL);

    pinMode(BUTTON_PIN, INPUT_PULLUP);

    WiFiManager.begin(httpServer, "TP-Link_6D94", "46039927");
    // WiFiManager.begin(httpServer, "GHOST_WIFI", "Wf108Ko76Mm009");
    
    oled->init();
    oled->setFont(Just_Another_Hand_Regular_50);

    if (WiFiManager.isConnected()) {
        httpServer->on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
            defaultController->mainRoute(request);
        });
        httpServer->on("/api/v1/temperature", HTTP_GET, [](AsyncWebServerRequest *request) {
            defaultController->temperatureRoute(request, temperature); 
        });
        httpServer->on("/api/v1/humidity", HTTP_GET, [](AsyncWebServerRequest *request) {
            defaultController->humidityRoute(request, humidity); 
        });
        httpServer->on("/api/v1/properties", HTTP_GET, [](AsyncWebServerRequest *request) {
            defaultController->propertiesRoute(request, temperature, humidity, brightness); 
        });
    }
    
    httpServer->onNotFound(notFound);
    AsyncElegantOTA.begin(httpServer, OTA_USERNAME, OTA_PASSWORD);
    httpServer->begin();
}

void loop() {
    temperature = humiditySensor->readTemperature();
    humidity = humiditySensor->readHumidity();

    drawValues(temperature, humidity);

    static int i = 0;
    if (i++ >= 100) {
        brightness = map(analogRead(A0), 0, 850, 100, 0);

        if (brightness < 0) brightness = 0;

        i = 0;
    }

    if(digitalRead(BUTTON_PIN) == LOW)
    {
        if (!pushButtonTime) {
            pushButtonTime = millis() / 1000; // seconds
        }

        // TODO: czym ma sterowac przycisk?


        if ((millis() / 1000) - pushButtonTime >= 1) {
            Serial.println("Wcisniete po 1 sec");
            // bedzie tutaj robil w kołko az nie wyjdzie z ifa, czy to bedzie problem?
        }

        if ((millis() / 1000) - pushButtonTime >= 5) {
            EepromStorage.clear();
            
            #if defined(ESP8266) || defined(ESP32)
                ESP.restart();
            #else
                resetItSelf();
            #endif
        }
    } else {
        pushButtonTime = 0;
    }
}