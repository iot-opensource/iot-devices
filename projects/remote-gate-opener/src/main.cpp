#include <Arduino.h>
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>
#include <WiFiManager.h>
#include <AsyncElegantOTA.h>
#include <Logger.h>
#include <persistance-systems/InMemorySystem.h>
#include <OneWire.h>
#include <DS18B20.h>
#include <Ticker.h>
#include "../include/consts.h"
#include "routing/DefaultController.h"
#include "routing/ActionsController.h"
#include "models/Gate.h"
#include "models/InsideThermometer.h"

Gate *gate;
AsyncWebServer *httpServer;
DefaultController *defaultController;
ActionsController *actionsController;
InsideThermometer *insideThermometer;
DS18B20 *outsideThermometer;
OneWire oneWire(PIN_THERMOMETER_OUTDOOR);
Ticker *timer;

void stopGate()
{
    gate->stop();
}

void notFound(AsyncWebServerRequest *request)
{
    request->send(404, "text/plain", "Not found");
}

void setup()
{
    Serial.begin(9600);

    Logger.begin(LogLevel::TRACE, new InMemorySystem());
    Logger.useSerial(&Serial);

    gate = new Gate(PIN_OPEN, PIN_CLOSE);
    httpServer = new AsyncWebServer(HTTP_SERVER_PORT);
    outsideThermometer = new DS18B20(&oneWire);
    insideThermometer = new InsideThermometer(PIN_THERMOMETER_INDOOR);
    timer = new Ticker(stopGate, OPEN_TIME_MS, MILLIS);
    defaultController = new DefaultController(gate, outsideThermometer, insideThermometer, timer);
    actionsController = new ActionsController(gate, timer);

    Logger.info("Remote gate opener");

    WiFiManager.begin(httpServer);
    Logger.useEndpoints(httpServer, "/logs");

    if (WiFiManager.isConnected()) {
        DateTime.begin();

        Logger.useDatetime(&DateTime);   
    }

    httpServer->on("/", [](AsyncWebServerRequest *request)
                   { defaultController->mainRoute(request); });
    httpServer->on("/api/v1/gate", HTTP_GET, [](AsyncWebServerRequest *request)
                   { defaultController->gateStatus(request); });
    httpServer->on("/api/v1/temperature-outside", HTTP_GET, [](AsyncWebServerRequest *request)
                   { defaultController->temperatureOutside(request); });
    httpServer->on("/api/v1/temperature-inside", HTTP_GET, [](AsyncWebServerRequest *request)
                   { defaultController->temperatureInside(request); });
    httpServer->on("/api/v1/properties", HTTP_GET, [](AsyncWebServerRequest *request)
                   { defaultController->propertiesRoute(request); });

    httpServer->on("/api/v1/actions/open", HTTP_POST, [](AsyncWebServerRequest *request)
                    { 
                       actionsController->openGateAction(request); 
                       Logger.info("Opened by API");
                    });
    httpServer->on("/api/v1/actions/close", HTTP_POST, [](AsyncWebServerRequest *request)
                    { 
                        actionsController->closeGateAction(request);
                        Logger.info("Closed by API");
                    });
    httpServer->on("/api/v1/actions/stop", HTTP_POST, [](AsyncWebServerRequest *request)
                    { 
                        actionsController->stopGateAction(request); 
                        Logger.info("Stopped by API");
                    });

    httpServer->begin();

    if (outsideThermometer->begin() == false)
    {
        Logger.error("ERROR: Outside thermometer not found");
    }

    pinMode(BUTTON_OPEN, INPUT);
    pinMode(BUTTON_CLOSE, INPUT);
    pinMode(BUTTON_STOP, INPUT);

    httpServer->onNotFound(notFound);
    AsyncElegantOTA.begin(httpServer);
    httpServer->begin();
}

void loop()
{
    if (digitalRead(BUTTON_OPEN) == HIGH && gate->getState() != GATE_OPEN && gate->getState() != GATE_OPENING) {
        delay(50);

        timer->start();
        gate->open();

        Logger.info("Opened by button or radio");

        while(digitalRead(BUTTON_OPEN) == HIGH);
    }

    if (digitalRead(BUTTON_CLOSE) == HIGH && gate->getState() != GATE_CLOSE && gate->getState() != GATE_CLOSING) {
        delay(50);

        timer->start();
        gate->close();

        Logger.info("Closed by button or radio");

        while(digitalRead(BUTTON_CLOSE) == HIGH);
    }

    if (digitalRead(BUTTON_STOP) == HIGH && gate->getState() != GATE_STOPPED) {
        delay(50);

        timer->stop();
        gate->stop();

        Logger.info("Stopped by button or radio");

        while(digitalRead(BUTTON_STOP) == HIGH);
    }

    if (timer->state() == RUNNING) {
        timer->update();
    }
}