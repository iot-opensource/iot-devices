#include "DefaultController.h"

DefaultController::DefaultController(Gate *gate, DS18B20 *outsideThermometer, InsideThermometer *insideThermometer, Ticker *timer)
{
    this->gate = gate;
    this->outsideThermometer = outsideThermometer;
    this->insideThermometer = insideThermometer;
    this->timer = timer;
}

DefaultController::~DefaultController()
{
}

void DefaultController::mainRoute(AsyncWebServerRequest *request)
{
    DynamicJsonDocument json(1024);
    json["name"] = "Remote Gate Opener";
    json["description"] = "";
    json["apiUrl"] = "/api/v1";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}

void DefaultController::gateStatus(AsyncWebServerRequest *request)
{
    DynamicJsonDocument json(1024);
    json["value"] = this->gate->stateAsString();
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}

void DefaultController::temperatureOutside(AsyncWebServerRequest *request)
{
    this->outsideThermometer->requestTemperatures();

    DynamicJsonDocument json(1024);
    json["value"] = String(this->outsideThermometer->getTempC());
    json["unit"] = "°C";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}

void DefaultController::temperatureInside(AsyncWebServerRequest *request)
{
    DynamicJsonDocument json(1024);
    json["value"] = String(this->insideThermometer->temperature());
    json["unit"] = "°C";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}

void DefaultController::propertiesRoute(AsyncWebServerRequest *request)
{
    DynamicJsonDocument json(1024);
    json["insideTemperature"]["value"] = String(this->insideThermometer->temperature());
    json["insideTemperature"]["unit"] = "°C";
    json["outsideTemperature"]["value"] = String(this->outsideThermometer->getTempC());
    json["outsideTemperature"]["unit"] = "°C";
    String parsedJson;
    serializeJson(json, parsedJson);

    request->send(200, "application/json", parsedJson);
}