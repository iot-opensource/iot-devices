#ifndef __DEFAULT_CONTROLLER_H__
#define __DEFAULT_CONTROLLER_H__

#ifndef Arduino_h
#include <Arduino.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include <DS18B20.h>
#include <Ticker.h>
#include "../models/Gate.h"
#include "../models/InsideThermometer.h"
#include "consts.h"

class DefaultController
{
    Gate *gate;
    DS18B20 *outsideThermometer;
    InsideThermometer *insideThermometer;
    Ticker *timer;

public:
    DefaultController(Gate *gate, DS18B20 *outsideThermometer, InsideThermometer *insideThermometer, Ticker *timer);
    ~DefaultController();
    void mainRoute(AsyncWebServerRequest *request);
    void gateStatus(AsyncWebServerRequest *request);
    void temperatureOutside(AsyncWebServerRequest *request);
    void temperatureInside(AsyncWebServerRequest *request);
    void propertiesRoute(AsyncWebServerRequest *request);
};

#endif