#include "ActionsController.h"

ActionsController::ActionsController(Gate *gate, Ticker *timer)
{
    this->gate = gate;
    this->timer = timer;
}

ActionsController::~ActionsController()
{
}

void ActionsController::openGateAction(AsyncWebServerRequest *request)
{
    this->gate->open();
    this->timer->start();

    request->send(202, "application/json");
}

void ActionsController::closeGateAction(AsyncWebServerRequest *request)
{
    this->gate->close();
    this->timer->start();

    request->send(202, "application/json");
}

void ActionsController::stopGateAction(AsyncWebServerRequest *request)
{
    this->gate->stop();
    this->timer->start();

    request->send(202, "application/json");
}