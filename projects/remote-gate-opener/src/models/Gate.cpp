#include "Gate.h"

Gate::Gate(uint8_t openPin, uint8_t closePin)
{
    this->closePin = closePin;
    this->openPin = openPin;
    this->state = GATE_STOPPED;

    pinMode(openPin, OUTPUT);
    pinMode(closePin, OUTPUT);
}

Gate::~Gate()
{
}

void Gate::open()
{
    digitalWrite(this->closePin, LOW);
    digitalWrite(this->openPin, HIGH);

    this->state = GATE_OPENING;
}

void Gate::close()
{
    digitalWrite(this->openPin, LOW);
    digitalWrite(this->closePin, HIGH);

    this->state = GATE_CLOSING;
}

void Gate::stop()
{
    digitalWrite(this->openPin, LOW);
    digitalWrite(this->closePin, LOW);

    this->state = GATE_STOPPED;
}

String Gate::stateAsString()
{
    if (this->state == GATE_OPEN)
    {
        return "open";
    }
    else if (this->state == GATE_OPENING)
    {
        return "opening";
    }
    else if (this->state == GATE_CLOSE)
    {
        return "close";
    }
    else if (this->state == GATE_CLOSING)
    {
        return "closing";
    }
    else
    {
        return "stopped";
    }
}

State Gate::getState()
{
    return this->state;
}