#include "InsideThermometer.h"

InsideThermometer::InsideThermometer(uint8_t analogPin)
{
    this->analogPin = analogPin;
}

InsideThermometer::~InsideThermometer()
{
}

float InsideThermometer::temperature() {
    int measurement = analogRead(this->analogPin);
    float input_voltage = (measurement * 3.3) / 1024.0;
    float temperature = (input_voltage - 0.5) / 0.01;

    return temperature;
}