#include "LedRgb.h"

LedRgb::LedRgb(uint8_t greenColorPin, uint8_t redColorPin, uint8_t blueColorPin)
{
    this->redColorPin = redColorPin;
    this->greenColorPin = greenColorPin;
    this->blueColorPin = blueColorPin;

    pinMode(redColorPin, INPUT_PULLUP);
    pinMode(greenColorPin, INPUT_PULLUP);
    pinMode(blueColorPin, INPUT_PULLUP);
}

LedRgb::~LedRgb()
{
}

Color LedRgb::getColor() {
    return this->color;
}

void LedRgb::changeColor(Color newColor) {
    this->color = newColor;

    // 255 -> full color ; 0 -> not visible
    analogWrite(this->redColorPin, 255 - this->color.red);
    analogWrite(this->greenColorPin, 255 - this->color.green);
    analogWrite(this->blueColorPin, 255 - this->color.blue);
}

void LedRgb::enable() {
    this->status = true;
    this->changeColor(this->color);
}

void LedRgb::disable() {
    this->status = false;

    analogWrite(this->redColorPin, 255);
    analogWrite(this->greenColorPin, 255);
    analogWrite(this->blueColorPin, 255);
}

bool LedRgb::isEnabled() {
    return this->status;
}