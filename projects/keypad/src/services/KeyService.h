#ifndef __KEY_SERVICE_H__
#define __KEY_SERVICE_H__

#ifndef Arduino_h
    #include <Arduino.h>
#endif
#include <ESP8266HTTPClient.h>
#include <EepromRepository.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include "consts.h"

class KeyService
{
    AsyncClient* httpClient;
public:
	KeyService(AsyncWebServer* httpServer);
	~KeyService();
    void runAction(int keyId);

    static String getWebhookId(String keyId, String param);
    static String getWebhookId(String keyId);
    static String getWebhookId(int keyId, String param);
    static String getWebhookId(int keyId);
};

#endif