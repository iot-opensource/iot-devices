#include "KeyService.h"

KeyService::KeyService(AsyncWebServer* httpServer) {
    this->httpClient = new AsyncClient();
}
KeyService::~KeyService() {}

void KeyService::runAction(int keyId) {
    EepromStorage.begin();

    String webhookId = getWebhookId(keyId, "action-url");

    Serial.println("runction first for key: " + String(keyId));

    if (EepromStorage.exists(webhookId)) {
        String actionUrl = EepromStorage.get<String>(webhookId);

        Serial.println(actionUrl);

        this->httpClient->onConnect([](void * arg, AsyncClient * client){
            Serial.println("Connected");
            client->onError(NULL, NULL);

            client->onDisconnect([](void * arg, AsyncClient * c){
            Serial.println("Disconnected");
            delete c;
            }, NULL);   

            client->onData([](void * arg, AsyncClient * c, void * data, size_t len){
            Serial.print("\r\nData: ");
            Serial.println(len);
            uint8_t * d = (uint8_t*)data;
            for(size_t i=0; i<len;i++)
                Serial.write(d[i]);
            }, NULL);

            //send the request
            char * request;
            sprintf(request, "GET / HTTP/1.0\r\nHost: %s\r\n\r\n", actionUrl.c_str());
            client->write(request);
        }, NULL);

        if (!this->httpClient->connect(actionUrl.c_str(), 80)) {
            Serial.println("Error");
        }

        // if (httpCode >= 200 && httpCode < 300) {
        //     String payload = this->httpClient->getString();

        //     Serial.println(payload);
        // } else {
        //     Serial.println("Error: " + this->httpClient->errorToString(httpCode));
        // }

        // this->httpClient->end();
    }
}

String KeyService::getWebhookId(int keyId) {
    return "webhook-" + String(keyId);
}

String KeyService::getWebhookId(int keyId, String param) {
    return "webhook-" + String(keyId) + "-" + param;
}

String KeyService::getWebhookId(String keyId) {
    return "webhook-" + keyId;
}

String KeyService::getWebhookId(String keyId, String param) {
    return "webhook-" + keyId + "-" + param;
}