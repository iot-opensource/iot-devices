#ifndef __DEFAULT_CONTROLLER_H__
#define __DEFAULT_CONTROLLER_H__

#ifndef Arduino_h
    #include <Arduino.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include "consts.h"

class DefaultController
{
public:
	DefaultController();
	~DefaultController();
    void mainRoute(AsyncWebServerRequest* request);
};

#endif