#include "DefaultController.h"

DefaultController::DefaultController()
{
}

DefaultController::~DefaultController()
{
}

void DefaultController::mainRoute(AsyncWebServerRequest* request)
{
    DynamicJsonDocument json(1024);
    json["name"] = "Keypad";
    json["description"] = "";
    json["apiUrl"] = "/api/v1";
    String parsedJson;
    serializeJson(json, parsedJson);
    
    request->send(200, "application/json", parsedJson);

}