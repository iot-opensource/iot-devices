#include "ConfigController.h"

ConfigController::ConfigController()
{
}

ConfigController::~ConfigController()
{
}

void ConfigController::getRoute(AsyncWebServerRequest* request)
{EepromStorage.begin();
    DynamicJsonDocument json(1024);
    JsonArray webhooks = json.createNestedArray("webhooks");

    for (int i = 1; i <= NUMBER_OF_KEYS; i++) {
        String key = KeyService::getWebhookId(i, "action-url");
        
        if (EepromStorage.exists(key)) {
            JsonObject obj = webhooks.createNestedObject();

            obj["keyId"] = i;
            obj["actionUrl"] = EepromStorage.get<String>(key);
        }
    }

    String parsedJson;
    serializeJson(json, parsedJson);
    request->send(200, "application/json", parsedJson);
}

void ConfigController::putRoute(AsyncWebServerRequest* request, const JsonObject& body)
{
    DynamicJsonDocument response(1024);
    String parsedJson;
    
    if (body.containsKey("webhooks") && body["webhooks"].is<JsonArray>()) {
        JsonArray webhooks = body["webhooks"].as<JsonArray>();

        EepromStorage.begin();

        for (unsigned int i = 0; i < webhooks.size(); i++) {
            if (!webhooks[i].containsKey("keyId") || !webhooks[i].containsKey("actionUrl")) {
                response["error"] = "Invalid parameters of webhook. See OpenAPI doc.";
                
                serializeJson(response, parsedJson);
                request->send(400, "application/json", parsedJson);
                
                return;
            }
            
            Serial.println(String(webhooks[i]["keyId"]));
            Serial.println(String(webhooks[i]["actionUrl"]));

            EepromStorage.set<String>(
                KeyService::getWebhookId(String(webhooks[i]["keyId"]), "action-url"), 
                String(webhooks[i]["actionUrl"])
            );
        }

        EepromStorage.apply();
    }
    response["ok"] = "ok";
    serializeJson(response, parsedJson);
    request->send(200, "application/json", parsedJson);
}