#ifndef __CONFIG_CONTROLLER_H__
#define __CONFIG_CONTROLLER_H__

#ifndef Arduino_h
    #include <Arduino.h>
#endif
#include <ESPAsyncWebServer.h>
#include <EepromRepository.h>
#include <ArduinoJson.h>
#include "../services/KeyService.h"
#include "../../include/consts.h"
#include "../../include/helpers.h"

class ConfigController
{
public:
	ConfigController();
	~ConfigController();
    void putRoute(AsyncWebServerRequest* request, const JsonObject& body);
    void getRoute(AsyncWebServerRequest* request);
};

#endif