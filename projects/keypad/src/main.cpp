#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Hash.h>
#include <PCF8574.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include <AsyncJson.h>
#include <EepromRepository.h>
#include <WiFiManager.h>
#include "../include/consts.h"
#include "routing/DefaultController.h"
#include "routing/ConfigController.h"
#include "../include/helpers.h"
#include "models/Device.h"
// #include "models/Config.h"

AsyncWebServer* httpServer;
DefaultController* defaultController;
ConfigController* configController;
Device* greenLed;
PCF8574* keypad;
KeyService * keyService;

int pushButtonTime = 0;

char keysMap[2] = { KEY1, KEY2 };

const char* PARAM_MESSAGE = "message";

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}

void log(String message) {
  Serial.println(message);
}

void setup() {
    Serial.begin(9600);
    Serial.setDebugOutput(true);

    EepromStorage.begin();

    httpServer = new AsyncWebServer(HTTP_SERVER_PORT);
    defaultController = new DefaultController();
    configController = new ConfigController();
    greenLed = new Device(GREEN_LED_PIN);
    keypad = new PCF8574(0x38);
    keyService = new KeyService(httpServer);

    pinMode(BUTTON_RESET, INPUT_PULLUP);
    
    for (unsigned int i = 0; i < sizeof(keysMap)/sizeof(*keysMap); i++) {
      keypad->pinMode(keysMap[i], INPUT);
    }

    if (!keypad->begin()) {
      log("error");
      log(String(keypad->getTransmissionStatusCode()));
    }

    WiFiManager.begin(httpServer);

    if (WiFiManager.isConnected()) {
        httpServer->on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
            defaultController->mainRoute(request);
        });
        httpServer->on("/api/v1/clear", HTTP_GET, [](AsyncWebServerRequest *request) {
            EepromStorage.clear();
            request->send(200, "text/html", "clear");
        });
        // httpServer->on("/api/v1/temperature", HTTP_GET, [](AsyncWebServerRequest *request) {
        //     defaultController->temperatureRoute(request, temperature); 
        // });
        httpServer->on("/api/v1/config", HTTP_GET, [](AsyncWebServerRequest *request) {
            configController->getRoute(request); 
        });
        
        httpServer->on("/api/v1/run", HTTP_GET, [](AsyncWebServerRequest *request) {
            keyService->runAction(1);
            configController->getRoute(request);
 
            request->send(200, "text/html", "ok");
        });

        // jak ustawic tutaj tylko put albo post
        AsyncCallbackJsonWebHandler *handler = new AsyncCallbackJsonWebHandler(
          "/api/v1/config",
          [](AsyncWebServerRequest *request,
          JsonVariant &json
        ) {
          DynamicJsonDocument body(1024);
          if (json.is<JsonArray>()) {
            body = json.as<JsonArray>();
          } else if (json.is<JsonObject>()) {
            body = json.as<JsonObject>();
          } else {
            body["error"] = "Invalid format";
            String response;
            serializeJson(body, response);
            request->send(400, "application/json", "Invalid format");

            return;
          }
          
          configController->putRoute(request, body.as<JsonObject>());
        });
        
        httpServer->addHandler(handler);
    } else {
      log("error");
    }

        
    httpServer->onNotFound(notFound);
    httpServer->begin();

    greenLed->enable();

    pinMode(BUTTON_RESET, INPUT_PULLUP);
}

void loop() {
  // if(digitalRead(BUTTON_RESET) == LOW)
  // {
  //     Serial.println("pushed");
  //     if (!pushButtonTime) {
  //         pushButtonTime = millis() / 1000; // seconds
  //     }

  //     if ((millis() / 1000) - pushButtonTime >= 5) {
  //         EepromStorage.clear();
  //         EepromStorage.apply();

  //         greenLed->enable();
  //         delay(200);
  //         greenLed->disable();
          
  //         resetItself();
  //     }
  // } else {
  //     pushButtonTime = 0;
  // }

  // musi byc przed odczytem stanu przyciskow bo inaczej klawisz numer 1 zawsze na starcie bedzie jako wcisniety
  // oraz pomiedzy odczytami tego samego pinu, bo inaczej stan jego bedzie oscylował pomiedzy 0 a 1
  // delay(50);

  // for (unsigned int i = 0; i < sizeof(keysMap)/sizeof(*keysMap); i++) {
  //   if (keypad->digitalRead(keysMap[i]) == 0) {
  //     log("Pressed key: " + String(i));
      
  //     while(keypad->digitalRead(keysMap[i]) == LOW) {ESP.wdtFeed();};
  //   }
  // }
}