#include <Arduino.h>
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Adafruit_Sensor.h>
#include <EEPROM.h>
#include "../include/consts.h"
#include "routing/DefaultController.h"
#include "models/Device.h"
#include "repository/DeviceEepromRepository.h"

Device* deviceOne;
ESP8266WebServer* httpServer;
DefaultController* defaultController;

void log(String message)
{
    Serial.println(message);
}

void setup() {
    EEPROM.begin(512);
    Serial.begin(9600);

    deviceOne = new Device(DEVICE_ONE_PIN);
    DeviceEepromRepository::readState() ? deviceOne->enable() : deviceOne->disable();

    httpServer = new ESP8266WebServer(HTTP_SERVER_PORT);
    defaultController = new DefaultController(httpServer, deviceOne);

    log("Smart Plug");

    WiFi.mode(WIFI_STA);
    WiFi.begin(STASSID, STAPSK);

    log("Wait for WiFi connection");

    while (WiFi.status() != WL_CONNECTED) {
        yield(); // this must be, becouse in other way watchdog reset all the system
    };

    log("WiFi connected!");

    if (MDNS.begin("esp8266")) {
        log("MDNS responder started");
    }

    httpServer->on("/", []() {
        defaultController->mainRoute(); 
    });
    httpServer->on("/api/v1/device", HTTP_GET, []() {
        defaultController->getDeviceRoute(); 
    });
    httpServer->on("/api/v1/device", HTTP_POST, []() {
        defaultController->postDeviceRoute(); 
    });

    httpServer->begin();
}

void loop() {
    httpServer->handleClient();
    MDNS.update();
}