#include "DefaultController.h"

DefaultController::DefaultController(ESP8266WebServer* httpServer, Device* deviceOne)
{
    this->httpServer = httpServer;
    this->deviceOne = deviceOne;
}

DefaultController::~DefaultController()
{
}

void DefaultController::mainRoute()
{
    DynamicJsonDocument json(1024);
    json["name"] = "Smart plug";
    json["description"] = "";
    json["apiUrl"] = "/api/v1";
    String parsedJson;
    serializeJson(json, parsedJson);
    
    this->httpServer->send(200, "text/plain",parsedJson);
}

void DefaultController::getDeviceRoute()
{
    DynamicJsonDocument json(1024);
    json["status"] = String(this->deviceOne->isEnabled());
    String parsedJson;
    serializeJson(json, parsedJson);

    this->httpServer->send(200, "application/json", parsedJson);
}

void DefaultController::postDeviceRoute()
{
    if (this->httpServer->hasArg("plain") == false) {
        this->httpServer->send(400, "application/json", "{\"status\":\"error\"}");
        return;
    }
 
    String body = this->httpServer->arg("plain");
 
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, body);

    if (error) {
        this->httpServer->send(400, "application/json", "{\"status\":\"error\"}");
        return;
    }
    
    bool status = doc["status"];
    DeviceEepromRepository::saveState(status);

    if (status) {
        this->deviceOne->enable();

        Serial.println("Device enabled");
    } else {
        this->deviceOne->disable();

        Serial.println("Device disabled");
    }

    this->getDeviceRoute();
}
