#ifndef __DEFAULT_CONTROLLER_H__
#define __DEFAULT_CONTROLLER_H__

#ifndef Arduino_h
    #include <Arduino.h>
#endif
#include <ESP8266WebServer.h>
#include <DHT.h>
#include <ArduinoJson.h>
#include <EEPROM.h>
#include "../models/Device.h"
#include "consts.h"
#include "../repository/DeviceEepromRepository.h"

class DefaultController
{
    ESP8266WebServer* httpServer;
    Device* deviceOne;

public:
	DefaultController(ESP8266WebServer* httpServer, Device* deviceOne);
	~DefaultController();
    void mainRoute();
    void getDeviceRoute();
    void postDeviceRoute();
};

#endif