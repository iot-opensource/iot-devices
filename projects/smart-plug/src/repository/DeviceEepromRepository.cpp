#include "DeviceEepromRepository.h"

DeviceEepromRepository::DeviceEepromRepository()
{
}

DeviceEepromRepository::~DeviceEepromRepository()
{
}

void DeviceEepromRepository::saveState(bool state)
{
    EEPROM.put(0, state);
    
    if (EEPROM.commit() == true)
    {
        Serial.println("Data saved.");
    } else {
        Serial.println("Error during save device state to EEPROM");
    }
}

bool DeviceEepromRepository::readState()
{
    bool state = false;

    EEPROM.get(0, state);

    return state;
}