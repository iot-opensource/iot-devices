#ifndef __DEVICE_EEPROM_REPOSITORY_H__
#define __DEVICE_EEPROM_REPOSITORY_H__

#include <Arduino.h>
#include <EEPROM.h>
#include "../../include/consts.h"

class DeviceEepromRepository
{
    public:
        DeviceEepromRepository();
        ~DeviceEepromRepository();
        static void saveState(bool state);
        static bool readState();
};

#endif