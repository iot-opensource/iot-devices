#include "EepromRepository.h"

EepromRepository::EepromRepository()
{
    this->jsonDocument = new DynamicJsonDocument(EEPROM_SIZE);
    this->isInitialized = false;
}

EepromRepository::~EepromRepository()
{
}

void EepromRepository::begin()
{
    if (this->isInitialized) return;

    String json = this->readEEPROM();

    deserializeJson(*this->jsonDocument, json);
    this->jsonObject = this->jsonDocument->as<JsonObject>();

    // If JSON couldn't be parsed,
    if (this->jsonObject.isNull()) {
        this->clear();
    }

    this->isInitialized = true;
}

bool EepromRepository::exists(String key)
{
    return this->jsonObject.containsKey(key);
}

void EepromRepository::clear()
{
    this->jsonDocument->clear();

    this->jsonObject = this->jsonDocument->to<JsonObject>();
}

void EepromRepository::clearEEPROM()
{
    #if defined(ESP8266) || defined(ESP32)
        EEPROM.begin(EEPROM_SIZE);
    #endif

    for (int i = 0; i < EEPROM_SIZE; i++) EEPROM.write(i, '\0');

    EEPROM.end();
}

void EepromRepository::remove(String key)
{
    this->jsonObject.remove(key);
}

void EepromRepository::apply()
{
    this->clearEEPROM();

    String json;
    serializeJson(*this->jsonDocument, json);

    this->writeEEPROM(json);
}

String EepromRepository::readEEPROM()
{
    #if defined(ESP8266) || defined(ESP32)
        EEPROM.begin(EEPROM_SIZE);
    #endif

    String json = "";
    unsigned char byte;
    
    for (unsigned int i = 0; i < EEPROM_SIZE; i++) {
        byte = EEPROM.read(i);
        if (byte == '\0') {
            break;
        } else {
            json += char(byte);
        }
    }

    EEPROM.end();

    return json;
}

void EepromRepository::writeEEPROM(String json) 
{
    #if defined(ESP8266) || defined(ESP32)
        EEPROM.begin(EEPROM_SIZE);
    #endif

    unsigned int i;

    for (i = 0; i < json.length(); i++) {
        EEPROM.write(i, json.charAt(i));
    }

    if (json.length() < EEPROM_SIZE)
        EEPROM.write(json.length(), '\0');

    EEPROM.end();
}