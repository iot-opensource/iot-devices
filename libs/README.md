# Arduino & ESP Libraries

## Publishing library
Login at first
```shell
pio account login
```
next
```shell
pio package publish
```

## Library file architecture
```bash
library-name
|__ include
|   |___ consts.h # file with constants variables
|__ src # application files
|__ examples
|   |___example-1
|   |   |___ main.cpp # file with example
|   |   |___ platformio.ini # definitions of framework, board, deps 
|__ library.json # manifest of library
|__ README.md # description of library
```