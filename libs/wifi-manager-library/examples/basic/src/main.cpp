#include <Arduino.h>
#include <ESPAsyncTCP.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>
#include <WiFiManager.h>

AsyncWebServer *httpServer;

void setup() {
    Serial.begin(9600);

    httpServer = new AsyncWebServer(80);

    WiFiManager.begin(httpServer);

    httpServer->on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, "text/html", "Work");
    });

    if (WiFiManager.isConnected()) {
        Serial.println("Connected.");
    } else {
        Serial.println("Not connected.");
    }

    httpServer->begin();
}

void loop() {
    if (WiFiManager.isConnected()) {
        Serial.print("RSSI: ");
        Serial.println(WiFi.RSSI());

        delay(500);
    }
}