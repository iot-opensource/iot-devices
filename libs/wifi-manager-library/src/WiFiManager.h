#ifndef __WIFI_MANAGER_H__
#define __WIFI_MANAGER_H__

#include <Arduino.h>
#include <EepromRepository.h>
#include <string.h>
#include "../view/view.h"
#include "../include/consts.h"

enum class PHYSICAL_MODE {
    B = 1, // max 11 Mb/s | 2,4 GHz
    G = 2, // max 54 Mb/s | 2,4 GHz
    N = 3  // 150 − 600 Mb/s | 2,4 GHz and 5GHz
};

class WiFiManagerLibrary
{
    bool connected;
    
    void runAccessPoint();
    void runAccessPointWithSingInForm(AsyncWebServer *httpServer);
    void enableClearEndpoint(AsyncWebServer *httpServer);

public:
    WiFiManagerLibrary();
    ~WiFiManagerLibrary();
    void begin(AsyncWebServer *httpServer);
    void begin(AsyncWebServer *httpServer, const char* ssid, const char* pass);
    void clear();
    bool isConnected();
    void setSleepMode(wifi_ps_type_t option);

    /**
     * @brief Set the power of transmission signal
     * 
     * @param dbm - logarytmiczna jednostka miary mocy w odniesieniu do 1 mW. Informuje nas o ile decybeli moc ta jest większa od 1 mW. P[dBm] = 10log_10(P[mW]/1mW)
     * max: 100 mW mocy nadawczej (20.5 dBm)
     */
    void setPower(float dbm);
    void setPhysicalMode(PHYSICAL_MODE mode);
};

static WiFiManagerLibrary WiFiManager;
extern WiFiManagerLibrary WiFiManager;

#endif