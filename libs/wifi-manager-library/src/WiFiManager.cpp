#include "WiFiManager.h"

WiFiManagerLibrary::WiFiManagerLibrary()
{
    this->connected = false;
}

WiFiManagerLibrary::~WiFiManagerLibrary() {}


void WiFiManagerLibrary::begin(AsyncWebServer *httpServer, const char* ssid, const char* pass)
{
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, pass); 

    this->setPower(WIFI_PS_NONE);
    this->enableClearEndpoint(httpServer);

    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
        Serial.printf("WiFi Failed!\n");
        return;
    }

    this->connected = true;
    this->connected = true;

    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());
}

void WiFiManagerLibrary::begin(AsyncWebServer *httpServer)
{
    EepromStorage.begin();

    if (!EepromStorage.exists("wifi-ssid")) {
        this->runAccessPointWithSingInForm(httpServer);
    } else {
        const char* ssid = EepromStorage.get<const char*>("wifi-ssid");
        const char* pass = EepromStorage.get<const char*>("wifi-pass");

        WiFi.mode(WIFI_STA);
        WiFi.begin(ssid, pass); 

        this->setPower(WIFI_PS_NONE);
        this->enableClearEndpoint(httpServer);

        if (WiFi.waitForConnectResult() != WL_CONNECTED) {
            Serial.printf("WiFi Failed!\n");
            this->runAccessPointWithSingInForm(httpServer);
            return;
        }

        this->connected = true;

        Serial.print("IP Address: ");
        Serial.println(WiFi.localIP());
    }
}

void WiFiManagerLibrary::clear() 
{
    EepromStorage.remove("wifi-ssid");
    EepromStorage.remove("wifi-pass");
    EepromStorage.apply();
}

void WiFiManagerLibrary::runAccessPoint() 
{
    IPAddress accessPointIp(1, 2, 3, 4);
    WiFi.softAPConfig(accessPointIp, accessPointIp, IPAddress(255, 255, 255, 0));
    WiFi.softAP(AP_SSID);
    
    Serial.print("AP ssid:\t");
    Serial.println(AP_SSID);
    Serial.print("IP address:\t");
    Serial.println(WiFi.softAPIP());
}

void WiFiManagerLibrary::runAccessPointWithSingInForm(AsyncWebServer *httpServer)
{
    this->runAccessPoint();

    httpServer->on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, "text/html", index_html);
    });

    httpServer->on("/", HTTP_POST, [](AsyncWebServerRequest *request) {
        if(!request->hasParam("ssid", true) || !request->hasParam("password", true)) {
            return request->send(400, "text/html", "Both parameters are needed.");
        }

        String ssid = request->getParam("ssid", true)->value();
        String password = request->getParam("password", true)->value();

        EepromStorage.set<String>("wifi-ssid", ssid);
        EepromStorage.set<String>("wifi-pass", password);
        EepromStorage.apply();

        #if defined(ESP8266) || defined(ESP32)
            ESP.restart();
        #else
            resetItSelf();
        #endif
    });
}

void WiFiManagerLibrary::enableClearEndpoint(AsyncWebServer *httpServer)
{
    httpServer->on("/clear", HTTP_GET, [](AsyncWebServerRequest *request) {
        WiFiManager.clear();

        #if defined(ESP8266) || defined(ESP32)
            ESP.restart();
        #else
            resetItSelf();
        #endif
    });
}

bool WiFiManagerLibrary::isConnected()
{
    return this->connected;
}

void WiFiManagerLibrary::setSleepMode(wifi_ps_type_t option) {
    WiFi.setSleep(option);
}

void WiFiManagerLibrary::setPower(float dbm) {
    if (dbm > 20.5) dbm = 20.5;
    if (dbm < 0) dbm = 0;

    #if defined(ESP8266) 
        WiFi.setOutputPower(dbm);
    #elif defined(ESP32)
        WiFi.setTxPower(dbm);
    #endif
}

void WiFiManagerLibrary::setPhysicalMode(PHYSICAL_MODE mode) {
    #if defined(ESP8266)

    WiFi.setPhyMode(static_cast<WiFiPhyMode_t>(mode));

    #endif
}