#include <Arduino.h>
#include <Logger.h>

void setup() {
    Serial.begin(9600);

    // Possible values: SILENT, TRACE, DEBUG, INFO, WARN, ERROR, FATAL
    Logger.begin(LogLevel::WARN);
    Logger.useSerial(&Serial);

    Logger.trace("This is %s message", "trace");
    Logger.debug("More important then %d message", 1);
    Logger.info("Information about %s and %s", "dog", "cat");
    Logger.warn("Warning");
    Logger.error("Someting went wrong because %s", "error");
    Logger.fatal("The worest message is %s", "fatal");
}

void loop() {}