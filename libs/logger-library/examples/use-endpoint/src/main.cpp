#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Hash.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ESPDateTime.h>
#include <Logger.h>
#include <DateTimeTZ.h>
#include <persistance-systems/InMemorySystem.h>

AsyncWebServer* httpServer;

void setup() {
    Serial.begin(9600);
    
    Logger.begin(LogLevel::TRACE, new InMemorySystem());
    Logger.useSerial(&Serial);
    Logger.error("Without datatime before");

    httpServer = new AsyncWebServer(80);
    
    WiFi.mode(WIFI_STA);
    WiFi.begin("GHOST_WIFI", "Wf108Ko76Mm009");

    if (WiFi.waitForConnectResult() == WL_CONNECTED) {
        httpServer->begin();
        
        DateTime.begin();
        // line below causes an exception, why?
        // DateTime.setTimeZone(TZ_Europe_Warsaw); // value from TZ.h file

        Logger.useEndpoints(httpServer);
        Logger.useDatetime(&DateTime);   

        Logger.info("IP Address: %s", WiFi.localIP().toString().c_str());
    } else {
        Logger.fatal("Cannot connect with Wi-Fi!");
    }
    
    Logger.trace("This is %s message", "trace");
    Logger.debug("More important then %d message", 1);
    Logger.info("Information about %s and %s", "dog", "cat");
    Logger.warn("Warning");
    Logger.error("Someting went wrong because %s", "error");
    Logger.fatal("The worest message is %s", "fatal");
}

void loop() {}