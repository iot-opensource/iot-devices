#ifndef __PERSISTANCE_SYSTEM_H__
#define __PERSISTANCE_SYSTEM_H__

#include <Arduino.h>
#include <vector>

using namespace std;

class PersistanceSystem
{
public:
    virtual ~PersistanceSystem() {};
    virtual void add(String log) = 0;
    virtual vector<String> readAll() = 0;
};

#endif