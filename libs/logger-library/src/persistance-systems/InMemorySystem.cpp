#include "InMemorySystem.h"

InMemorySystem::InMemorySystem(unsigned int limit) {
    this->limit = limit;
}

InMemorySystem::~InMemorySystem() {}

void InMemorySystem::add(String log) {
    if (this->logs.size() >= this->limit) {
        this->logs.erase(this->logs.begin());
    }

    this->logs.push_back(log);
}

vector<String> InMemorySystem::readAll() {
    return this->logs;
}