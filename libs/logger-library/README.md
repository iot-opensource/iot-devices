# Logger Library for Arduino, ESP

## Review of solutions

There are currently several logging libraries for microcontrollers on the market. The most popular of them is https://github.com/thijse/Arduino-Log. Despite the many advantages, such as logging levels, of including variable values in messages as parameters, modifying the display format, has some limitations. Transport of logs to an external destination or database is not possible there. Also, there is no integration with REST API, which would allow you to easily view the logs.

## Decision

I decided to write my own logger library. I moved all the necessary functions from ArduinoLog to my library, of course, writing them from the beginning by myself, not copying.

## Features

LoggerLibrary in basic mode is very easy to use. An example is in `examples/basic/src/main.cpp`. By default, library not store any logs in memory and the default output is Serial Monitor.

In my library, I have separated log storage from the output where they are available. We can only store in one place. This is limited by several problems that occur when the same logs are stored in many places, e.g. the problem of data inconsistency. On the other hand, logs can be viewed in many places, such as SerialMonitor or REST API. More information in `examples/*/src/main.cpp`.

## To do

Persistance Systems:
- In Memory ✅
- Flash
- SD Card
- Remote (Elastic Search, Grafana Loki)